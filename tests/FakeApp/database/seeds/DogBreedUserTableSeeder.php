<?php

namespace Leordex\LaravelPlainSQL\Tests\FakeApp\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DogBreedUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dog_breed_user')->truncate();
        DB::table('dog_breed_user')->insert([
            [
                'dog_breed_id' => 2,
                'user_id' => 1
            ],
            [
                'dog_breed_id' => 3,
                'user_id' => 1
            ]
        ]);
    }
}
