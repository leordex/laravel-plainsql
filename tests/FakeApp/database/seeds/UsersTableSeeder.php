<?php

namespace Leordex\LaravelPlainSQL\Tests\FakeApp\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert(
            [
                'id' => 1,
                'name' => 'vi',
                'phone' => '+74621348789',
                'password' => '$2y$10$afd//MOyZkRklbU8zd1HKuJ4NUsyP8jjJ6pgwXTtjAp6kr5H5reiW',
                'logins' => 0,
                'created_at' => '2016-07-07 15:57:05',
                'updated_at' => '2016-09-02 12:36:18'
            ]
        );
    }
}
