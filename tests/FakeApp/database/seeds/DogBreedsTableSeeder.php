<?php

namespace Leordex\LaravelPlainSQL\Tests\FakeApp\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DogBreedsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dog_breeds')->truncate();
        DB::table('dog_breeds')->insert([
            [
                'id' => 2,
                'breed' => 'akita-inu',
                'best' => 1,
                'created_at' => '2018-07-07 15:57:05',
                'updated_at' => '2018-09-02 12:36:18'
            ],
            [
                'id' => 3,
                'breed' => 'bolonka',
                'best' => 0,
                'created_at' => '2018-07-07 15:57:05',
                'updated_at' => '2018-09-02 12:36:18'
            ]
        ]);
    }
}
