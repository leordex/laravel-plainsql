<?php

namespace Leordex\LaravelPlainSQL\Tests;

use Leordex\LaravelPlainSQL\LaravelPlainSQLServiceProvider;
use Orchestra\Database\ConsoleServiceProvider;
use Orchestra\Testbench\Console\Kernel;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    protected function getPackageProviders($app)
    {
        return [
            LaravelPlainSQLServiceProvider::class,
            ConsoleServiceProvider::class
        ];
    }

    /**
     * Resolve application Console Kernel implementation.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function resolveApplicationConsoleKernel($app)
    {
        $app->singleton('Illuminate\Contracts\Console\Kernel', Kernel::class);
    }
}