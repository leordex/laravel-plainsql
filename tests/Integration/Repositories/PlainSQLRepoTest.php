<?php

namespace Leordex\LaravelPlainSQL\Tests\Integration\Repositories;

use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Query\Grammars\Grammar;
use Leordex\LaravelPlainSQL\Repositories\PlainSQLRepo;
use Leordex\LaravelPlainSQL\Tests\FakeApp\Database\Seeds\UsersTableSeeder;
use Leordex\LaravelPlainSQL\Tests\Integration\TestCaseIntegration;

class PlainSQLRepoTest extends TestCaseIntegration
{
    private $plainSQLRepo;

    public function setUp()
    {
        parent::setUp();

        $this->seed(UsersTableSeeder::class);

        $connection = app()->make(ConnectionInterface::class);
        $grammar = app()->make(Grammar::class);

        $this->plainSQLRepo = new PlainSQLRepo($connection, $grammar);
    }

    public function testInsert()
    {
        $userData = [
            'id' => 5,
            'name' => 'Cooler',
            'phone' => '+74392349049',
            'password' => 'blabla'
        ];

        $result = $this->plainSQLRepo->insert(
            'users',
            $userData,
            ['id']
        );
        $this->assertFalse($result);

        $result = $this->plainSQLRepo->insert(
            'users',
            $userData,
            [],
            true
        );
        $this->assertFalse($result);

        $result = $this->plainSQLRepo->insert(
            'users',
            []
        );
        $this->assertFalse($result);

        $result = $this->plainSQLRepo->insert(
            'users',
            $userData
        );
        $this->assertTrue($result);
    }

    public function testSelect()
    {
        $query =
<<<SQL
SELECT `users`.* FROM `users` WHERE `users`.`created_at` > :createdFrom AND `users`.`created_at` < :createdTo ORDER BY `users`.`id`
SQL;
        $params = [
            'createdFrom' => '2016-01-01 00:00:00',
            'createdTo' => '2017-01-01 00:00:00'
        ];

        $result = $this->plainSQLRepo->select($query, $params);
        $this->assertEquals(1, $result['id']);
    }

    public function testSelectMany()
    {
        $query =
            <<<SQL
SELECT `users`.* FROM `users` WHERE `users`.`created_at` > :createdFrom AND `users`.`created_at` < :createdTo ORDER BY `users`.`id`
SQL;
        $params = [
            'createdFrom' => '2016-01-01 00:00:00',
            'createdTo' => '2017-01-01 00:00:00'
        ];

        $result = $this->plainSQLRepo->selectMany($query, $params);
        $this->assertEquals(1, $result[0]['id']);
    }
}