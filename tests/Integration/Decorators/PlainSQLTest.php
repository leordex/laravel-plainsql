<?php

namespace Leordex\LaravelPlainSQL\Tests\Integration\Decorators;

use Illuminate\Filesystem\Filesystem;
use Leordex\LaravelPlainSQL\Tests\FakeApp\Database\Seeds\DogBreedsTableSeeder;
use Leordex\LaravelPlainSQL\Tests\FakeApp\Database\Seeds\DogBreedUserTableSeeder;
use Leordex\LaravelPlainSQL\Tests\FakeApp\Database\Seeds\UsersTableSeeder;
use Leordex\LaravelPlainSQL\Tests\Integration\TestCaseIntegration;
use Leordex\LaravelPlainSQL\Decorators\PlainSQL;

class PlainSQLTest extends TestCaseIntegration
{
    /** @var Filesystem */
    private $files;

    public function setUp()
    {
        parent::setUp();

        $this->seed(UsersTableSeeder::class);
        $this->seed(DogBreedsTableSeeder::class);
        $this->seed(DogBreedUserTableSeeder::class);

        $this->files = app()->make(Filesystem::class);
    }

    public function testImportSQL()
    {
        $sql =
<<<SQL
SELECT
       `users`.*,
       `dog_breeds`.`id` as `dog_breeds_id`,
       `dog_breeds`.`breed` as `dog_breeds_breed`
FROM `users`
  INNER JOIN `dog_breed_user`
    ON `dog_breed_user`.`user_id` = `users`.`id`
  INNER JOIN `dog_breeds`
    ON `dog_breeds`.`id` = `dog_breed_user`.`dog_breed_id`
WHERE `dog_breeds`.`best` = :bestBreed {{andWhere}}
ORDER BY {{orderBy}}
LIMIT :offset,:limit
SQL;

        $expectedQuery =
<<<SQL
SELECT `users`.*, `dog_breeds`.`id` as `dog_breeds_id`, `dog_breeds`.`breed` as `dog_breeds_breed` FROM `users` INNER JOIN `dog_breed_user` ON `dog_breed_user`.`user_id` = `users`.`id` INNER JOIN `dog_breeds` ON `dog_breeds`.`id` = `dog_breed_user`.`dog_breed_id` WHERE `dog_breeds`.`best` = :bestBreed AND `users`.`created_at` > :createdFrom AND `users`.`created_at` < :createdTo ORDER BY `users`.`id` DESC LIMIT :offset,:limit
SQL;

        $directory = app_path() . "/" . "PlainSQL";
        $file = $directory . "/UserWithHisBestBreed.sql";

        $queryAndWhere = implode('', [
            "\r\nAND `users`.`created_at` > :createdFrom",
            "\r\nAND `users`.`created_at` < :createdTo"
        ]);

        $orderByResult = "`users`.`id` DESC";

        /** @var PlainSQL */
        $class = new class($this->files) extends PlainSQL {
            public function __construct(Filesystem $files)
            {
                parent::__construct($files);
            }

            public function test(string $name, array $params = [])
            {
                return parent::importSql($name, $params);
            }
        };

        $query = $class->test(
            'UserWithHisBestBreed',
            [
                'andWhere' => $queryAndWhere,
                'orderBy' => $orderByResult
            ]
        );
        $this->assertEquals("", $query);

        $this->files->makeDirectory($directory);
        $this->files->put($file, $sql);

        $query = $class->test(
            'UserWithHisBestBreed',
            [
                'andWhere' => $queryAndWhere,
                'orderBy' => $orderByResult
            ]
        );
        $this->assertEquals($expectedQuery, $query);

        $this->files->deleteDirectory($directory);
    }
}