<?php

namespace Leordex\LaravelPlainSQL\Tests\Integration;

use Leordex\LaravelPlainSQL\Tests\TestCase;

abstract class TestCaseIntegration extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        try {
            $this->loadMigrationsFrom(
                realpath(
                    __DIR__ . "/../FakeApp/database/migrations"
                )
            );
        } catch(\Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    protected function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);
        $app->setBasePath(__DIR__ . "/../FakeApp");
        $app['config']->set('plainsql.sql_location', 'PlainSQL');
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
