<?php

namespace Leordex\LaravelPlainSQL;

use Illuminate\Support\ServiceProvider;
use Leordex\LaravelPlainSQL\Contracts\PlainSQLRepoInterface;
use Leordex\LaravelPlainSQL\Repositories\PlainSQLRepo;

class LaravelPlainSQLServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/config/plainsql.php' => config_path('plainsql.php'),
            ], 'laravel-plainsql');
        }
        $this->loadTranslationsFrom(
            __DIR__ . '/resources/lang',
            'laravel-plainsql'
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/plainsql.php', 'laravel-plainsql');
        $this->app->singleton(PlainSQLRepoInterface::class, PlainSQLRepo::class);
    }
}
