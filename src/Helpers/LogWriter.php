<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 19.12.2017
 * Time: 14:32
 */

namespace Leordex\LaravelPlainSQL\Helpers;

use Illuminate\Support\Facades\Log;

class LogWriter
{
    public static function onException($error, \Exception $e, $trace = false)
    {
        $data = [
            'message' => $e->getMessage(),
            'errorCode' => $e->getCode(),
            'line' => $e->getFile() . " " . $e->getLine()
        ];

        Log::error($error, $data);
    }

    public static function onUserError($error, $file, $line)
    {
        Log::error($error, [
            'line' => $file . " " . $line
        ]);
    }

    public static function onUserWarning($warning, $file, $line)
    {
        Log::warning($warning, [
            'line' => $file . " " . $line
        ]);
    }
}