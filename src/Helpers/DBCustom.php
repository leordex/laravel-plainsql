<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 05.06.2017
 * Time: 11:40
 */

namespace Leordex\LaravelPlainSQL\Helpers;


class DBCustom
{
    public static function dataWithPattern(array &$item, $name = 'pivot')
    {
        $data = array_filter($item, function($value) use ($name) {
            return fnmatch($name . '_*', $value);
        }, ARRAY_FILTER_USE_KEY);

        $item = array_except($item, array_keys($data));
        foreach ($data as $key => $value) {
            $data[str_replace($name . '_', '', $key)] = $value;
            unset($data[$key]);
        }

        return $data;
    }
}