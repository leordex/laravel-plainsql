<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 19.12.2017
 * Time: 14:32
 */

namespace Leordex\LaravelPlainSQL\Helpers;

class Common
{
    public static function getSQLBasePath()
    {
        $location = config('plainsql.sql_location');
        if (! $location) {
            abort(
                500,
                trans('laravel-plainsql::messages.sql_location_absent')
            );
        }
        return str_replace(
            "\\",
            "/",
            trim(
                $location,
                "/\\"
            )
        );
    }
}