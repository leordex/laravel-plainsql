<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 11.07.2018
 * Time: 17:04
 */

namespace Leordex\LaravelPlainSQL\Repositories;

use Leordex\LaravelPlainSQL\Contracts\PlainSQLRepoInterface;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\MySqlConnection;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Query\Grammars\Grammar;
use Illuminate\Support\Collection;
use Leordex\LaravelPlainSQL\Helpers\LogWriter;

class PlainSQLRepo implements PlainSQLRepoInterface
{
    /**
     * The database connection instance.
     *
     * @var \Illuminate\Database\ConnectionInterface
     */
    public $connection;

    /**
     * The database query grammar instance.
     *
     * @var \Illuminate\Database\Query\Grammars\Grammar
     */
    public $grammar;

    /**
     * Create a new query builder instance.
     *
     * @param  \Illuminate\Database\ConnectionInterface  $connection
     * @param  \Illuminate\Database\Query\Grammars\Grammar  $grammar
     */
    public function __construct(
        ConnectionInterface $connection,
        Grammar $grammar = null
    ) {
        $this->connection = $connection;
        $this->grammar = $grammar ?: $connection->getQueryGrammar();
    }

    public function insert(
        string $tableName,
        array $values,
        array $updateOnDuplicateFields = [],
        bool $ignore = false
    ): bool {
        if (
            $ignore &&
            ! $this->connection instanceof MySqlConnection
        ) {
            LogWriter::onUserWarning(
                trans('laravel-plainsql::messages.ignore_mysql_only'),
                __FILE__,
                __LINE__ - 6
            );
            return false;
        }

        if (
            $updateOnDuplicateFields &&
            ! $this->connection instanceof MySqlConnection
        ) {
            LogWriter::onUserWarning(
                trans('laravel-plainsql::messages.duplicate_mysql_only'),
                __FILE__,
                __LINE__ - 6
            );
            return false;
        }

        if (! $values) {
            return false;
        }

        if (! is_array(reset($values))) {
            $values = [$values];
        }

        $table = $this->grammar->wrapTable($tableName);
        $columns = $this->grammar->columnize(
            array_keys(
                reset($values)
            )
        );
        $parameters = collect($values)
            ->map(
                function ($record) {
                    return "({$this->grammar->parameterize($record)})";
                }
            )->implode(', ');

        $insert = $ignore ? 'insert ignore' : 'insert';
        $postInsert = '';

        if ($updateOnDuplicateFields) {
            $duplicateFields = collect($updateOnDuplicateFields)
                ->map(
                    function ($field) {
                        return "`{$field}` = values(`{$field}`)";
                    }
                )->implode(', ');
            $postInsert = " on duplicate key update {$duplicateFields}";
        }

        return $this->connection->insert(
            str_replace(
                "\"",
                "`",
                "{$insert} into {$table} ({$columns}) " .
                "values {$parameters}{$postInsert}"),
            $this->cleanBindings(array_flatten($values, 1))
        );
    }

    public function select(string $query, array $bindings = []): Collection
    {
        $records = $this->connection
            ->select($query, $bindings);
        return collect(
            array_shift($records)
        );
    }

    public function selectMany(string $query, array $bindings = []): Collection
    {
         return collect(
            array_map(
                function($value) {
                    return (array) $value;
                },
                $this->connection
                    ->select($query, $bindings)
            )
         );
    }

    public function cursor(string $query, array $bindings = []): \Generator
    {
        if (! $this->connection instanceof MySqlConnection) {
            LogWriter::onUserWarning(
                trans('laravel-plainsql::messages.cursor_mysql_only'),
                __FILE__,
                __LINE__ - 4
            );
            return null;
        }
        /** @var MySqlConnection $connection */
        $connection = $this->connection;

        return $connection->cursor($query, $bindings);
    }

    /**
     * Remove all of the expressions from a list of bindings.
     *
     * @param  array  $bindings
     * @return array
     */
    protected function cleanBindings(array $bindings)
    {
        return array_values(
            array_filter(
                $bindings,
                function ($binding) {
                    return ! $binding instanceof Expression;
                }
            )
        );
    }
}