<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 31.01.2018
 * Time: 14:40
 */

namespace Leordex\LaravelPlainSQL\Contracts;


use Illuminate\Support\Collection;

interface PlainSQLRepoInterface
{
    public function insert(
        string $tableName,
        array $values,
        array $updateOnDuplicateFields = [],
        bool $ignore = false
    ): bool;

    public function select(string $query, array $bindings = []): Collection;

    public function selectMany(string $query, array $bindings = []): Collection;

    public function cursor(string $query, array $bindings = []): \Generator;
}