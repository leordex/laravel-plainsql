<?php
/**
 * Created by PhpStorm.
 * User: Leordex
 * Date: 05.03.2018
 * Time: 16:57
 *
 */

namespace Leordex\LaravelPlainSQL\Decorators;

use Illuminate\Filesystem\Filesystem;
use Leordex\LaravelPlainSQL\Contracts\PlainSQLRepoInterface;
use Leordex\LaravelPlainSQL\Helpers\Common;
use Leordex\LaravelPlainSQL\Helpers\LogWriter;

abstract class PlainSQL
{
    private $pathToSQL;

    private $plainSQLRepo;

    /** @var Filesystem */
    private $files;

    public function __construct(Filesystem $files)
    {
        $this->plainSQLRepo = app()->make(
            PlainSQLRepoInterface::class
        );
        $this->pathToSQL = app_path() . "/" . Common::getSQLBasePath() . "/";
        $this->files = $files;
    }

    protected function repo(): PlainSQLRepoInterface
    {
        return $this->plainSQLRepo;
    }

    protected function importSql(string $name, array $params = []): string
    {
        $filePath = $this->pathToSQL . $name . '.sql';
        if (! $this->files->exists($filePath)) {
            LogWriter::onUserError(
                trans('laravel-plainsql::messages.sql_not_found'),
                __FILE__,
                __LINE__ - 5
            );
            return '';
        }

        $sql = null;
        try {
            $sql = $this->files->get($filePath);
        } catch (\Exception $e) {
            LogWriter::onException(
                trans('laravel-plainsql::messages.sql_not_found'),
                $e
            );
        }

        if (! $params) {
            return $sql;
        }

        $search = array_map(
            function($value) {
                return "{{" . $value . "}}";
            },
            array_keys($params)
        );

        return trim(
            preg_replace(
                "/\s+/",
                " ",
                str_replace(
                    "\r\n",
                    " ",
                    str_replace(
                        $search,
                        array_values($params),
                        $sql
                    )
                )
            )
        );
    }
}