<?php

return [
    'cursor_mysql_only' => "'cursor' can be used with MySQL only",
    'ignore_mysql_only' => "'insert ignore' can be used with MySQL only",
    'duplicate_mysql_only' => "'update on duplicate fields' can be used with MySQL only",
    'sql_location_absent' => "'sql_location' parameter cannot be empty",
    'sql_not_found' => 'SQL file not found'
];
