<?php

return [
    'cursor_mysql_only' => "'cursor' можно использовать только с MySQL",
    'ignore_mysql_only' => "'insert ignore' можно использовать только с MySQL",
    'duplicate_mysql_only' => "'update on duplicate fields' можно использовать только с MySQL",
    'sql_location_absent' => "Параметр 'sql_location' не может быть пустым",
    'sql_not_found' => 'Файл SQL не найден'
];
